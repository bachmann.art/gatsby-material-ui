import React from 'react'
import Layout from '../components/layout'
import IOSAndroidPage from '../components/ui/servicesPage/IOSAndroidPage'

const ios_android = () => {
  return (
    <Layout>
      <IOSAndroidPage />
    </Layout>
  )
}

export default ios_android
