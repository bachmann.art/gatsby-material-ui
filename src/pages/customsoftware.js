import React from 'react'
import Layout from '../components/layout'
import CustomSoftware from '../components/ui/servicesPage/CustomSoftware'

const customsoftware = () => {
  return (
    <Layout>
      <CustomSoftware />
    </Layout>
  )
}

export default customsoftware
