import React from 'react'
import Layout from '../components/layout'
import WebDevelopment from '../components/ui/servicesPage/WebDevelopment'

const web_development = () => {
  return (
    <Layout>
      <WebDevelopment />
    </Layout>
  )
}

export default web_development
