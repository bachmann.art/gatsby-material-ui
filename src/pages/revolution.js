import React from 'react'
import Layout from '../components/layout'
import Revolution from '../components/ui/revolutionPage/Revolution'

const revolution = () => {
  return (
    <Layout>
      <Revolution />
    </Layout>
  )
}

export default revolution
