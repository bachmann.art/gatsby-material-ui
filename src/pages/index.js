import React from 'react'
import LandingPage from '../components/ui/landingPage/LandingPage'

import Layout from '../components/layout'



const IndexPage = () => (
  <Layout>
    <LandingPage />
  </Layout>
)

export default IndexPage
