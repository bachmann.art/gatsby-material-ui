import React from 'react'
import Layout from '../components/layout'
import ServicesPage from '../components/ui/servicesPage/ServicesPage'

const services = () => {
  return (
    <Layout>
      <ServicesPage />
    </Layout>
  )
}

export default services
