import React from 'react'
import Layout from '../components/layout'
import AboutUs from '../components/ui/aboutUsPage/AboutUs'

const aboutus = () => {
  return (
    <Layout>
      <AboutUs />
    </Layout>
  )
}

export default aboutus
