import React from 'react'

const LongText = () => {
  return (
    <div>
      <p>I don't like being outdoors, Smithers. For one thing, there's too many fat children.
      Bart, with $10,000 we'd be millionaires! We could buy all kinds of useful things like…love! Uh, no, you got the wrong number. This is 9-1…2. Kids, kids. I'm not going to die. That only happens to bad people.

      Shoplifting is a victimless crime. Like punching someone in the dark. Oh, loneliness and cheeseburgers are a dangerous mix. I'm normally not a praying man, but if you're up there, please save me, Superman.

      What good is money if it can't inspire terror in your fellow man?
      Remember the time he ate my goldfish? And you lied and said I never had goldfish. Then why did I have the bowl, Bart? *Why did I have the bowl?* I like my beer cold, my TV loud and my homosexuals flaming.

      Fat Tony is a cancer on this fair city! He is the cancer and I am the…uh…what cures cancer?
      …And the fluffy kitten played with that ball of string all through the night. On a lighter note, a Kwik-E-Mart clerk was brutally murdered last night.
      Here's to alcohol, the cause of — and solution to — all life's problems.
      No children have ever meddled with the Republican Party and lived to tell about it.
      A woman is a lot like a refrigerator. Six feet tall, 300 pounds…it makes ice. Books are useless! I only ever read one book, "To Kill A Mockingbird," and it gave me absolutely no insight on how to kill mockingbirds! Sure it taught me not to judge a man by the color of his skin…but what good does *that* do me?

      Duffman can't breathe! OH NO!
      Bart, with $10,000 we'd be millionaires! We could buy all kinds of useful things like…love!
      I'm allergic to bee stings. They cause me to, uh, die.
      Please do not offer my god a peanut. Yes! I am a citizen! Now which way to the welfare office? I'm kidding, I'm kidding. I work, I work. What's the point of going out? We're just going to wind up back here anyway.

      Save me, Jeebus. A woman is a lot like a refrigerator. Six feet tall, 300 pounds…it makes ice. Mrs. Krabappel and Principal Skinner were in the closet making babies and I saw one of the babies and then the baby looked at me.

      Kids, kids. I'm not going to die. That only happens to bad people. Dear Mr. President, There are too many states nowadays. Please, eliminate three. P.S. I am not a crackpot. I've had it with this school, Skinner. Low test scores, class after class of ugly, ugly children…

      Kids, kids. I'm not going to die. That only happens to bad people. I'm going to the back seat of my car, with the woman I love, and I won't be back for ten minutes! I've done everything the Bible says — even the stuff that contradicts the other stuff!

      What good is money if it can't inspire terror in your fellow man? No children have ever meddled with the Republican Party and lived to tell about it. I didn't get rich by signing checks. Oh, everything looks bad if you remember it.

      Son, when you participate in sporting events, it's not whether you win or lose: it's how drunk you get. Fat Tony is a cancer on this fair city! He is the cancer and I am the…uh…what cures cancer?

      And here I am using my own lungs like a sucker. …And the fluffy kitten played with that ball of string all through the night. On a lighter note, a Kwik-E-Mart clerk was brutally murdered last night.

      Dear Mr. President, There are too many states nowadays. Please, eliminate three. P.S. I am not a crackpot. And now, in the spirit of the season: start shopping. And for every dollar of Krusty merchandise you buy, I will be nice to a sick kid. For legal purposes, sick kids may include hookers with a cold.

      I hope this has taught you kids a lesson: kids never learn. Please do not offer my god a peanut. Lisa, vampires are make-believe, like elves, gremlins, and Eskimos. Duffman can't breathe! OH NO! I hope this has taught you kids a lesson: kids never learn.

      This is the greatest case of false advertising I've seen since I sued the movie "The Never Ending Story." I'm normally not a praying man, but if you're up there, please save me, Superman. I'm allergic to bee stings. They cause me to, uh, die.

      Whoa, slow down there, maestro. There's a *New* Mexico? What good is money if it can't inspire terror in your fellow man? When I held that gun in my hand, I felt a surge of power…like God must feel when he's holding a gun.
      I don't like being outdoors, Smithers. For one thing, there's too many fat children.
      Bart, with $10,000 we'd be millionaires! We could buy all kinds of useful things like…love! Uh, no, you got the wrong number. This is 9-1…2. Kids, kids. I'm not going to die. That only happens to bad people.

      Shoplifting is a victimless crime. Like punching someone in the dark. Oh, loneliness and cheeseburgers are a dangerous mix. I'm normally not a praying man, but if you're up there, please save me, Superman.

      What good is money if it can't inspire terror in your fellow man?
      Remember the time he ate my goldfish? And you lied and said I never had goldfish. Then why did I have the bowl, Bart? *Why did I have the bowl?* I like my beer cold, my TV loud and my homosexuals flaming.

      Fat Tony is a cancer on this fair city! He is the cancer and I am the…uh…what cures cancer?
      …And the fluffy kitten played with that ball of string all through the night. On a lighter note, a Kwik-E-Mart clerk was brutally murdered last night.
      Here's to alcohol, the cause of — and solution to — all life's problems.
      No children have ever meddled with the Republican Party and lived to tell about it.
      A woman is a lot like a refrigerator. Six feet tall, 300 pounds…it makes ice. Books are useless! I only ever read one book, "To Kill A Mockingbird," and it gave me absolutely no insight on how to kill mockingbirds! Sure it taught me not to judge a man by the color of his skin…but what good does *that* do me?

      Duffman can't breathe! OH NO!
      Bart, with $10,000 we'd be millionaires! We could buy all kinds of useful things like…love!
      I'm allergic to bee stings. They cause me to, uh, die.
      Please do not offer my god a peanut. Yes! I am a citizen! Now which way to the welfare office? I'm kidding, I'm kidding. I work, I work. What's the point of going out? We're just going to wind up back here anyway.

      Save me, Jeebus. A woman is a lot like a refrigerator. Six feet tall, 300 pounds…it makes ice. Mrs. Krabappel and Principal Skinner were in the closet making babies and I saw one of the babies and then the baby looked at me.

      Kids, kids. I'm not going to die. That only happens to bad people. Dear Mr. President, There are too many states nowadays. Please, eliminate three. P.S. I am not a crackpot. I've had it with this school, Skinner. Low test scores, class after class of ugly, ugly children…

      Kids, kids. I'm not going to die. That only happens to bad people. I'm going to the back seat of my car, with the woman I love, and I won't be back for ten minutes! I've done everything the Bible says — even the stuff that contradicts the other stuff!

      What good is money if it can't inspire terror in your fellow man? No children have ever meddled with the Republican Party and lived to tell about it. I didn't get rich by signing checks. Oh, everything looks bad if you remember it.

      Son, when you participate in sporting events, it's not whether you win or lose: it's how drunk you get. Fat Tony is a cancer on this fair city! He is the cancer and I am the…uh…what cures cancer?

      And here I am using my own lungs like a sucker. …And the fluffy kitten played with that ball of string all through the night. On a lighter note, a Kwik-E-Mart clerk was brutally murdered last night.

      Dear Mr. President, There are too many states nowadays. Please, eliminate three. P.S. I am not a crackpot. And now, in the spirit of the season: start shopping. And for every dollar of Krusty merchandise you buy, I will be nice to a sick kid. For legal purposes, sick kids may include hookers with a cold.

      I hope this has taught you kids a lesson: kids never learn. Please do not offer my god a peanut. Lisa, vampires are make-believe, like elves, gremlins, and Eskimos. Duffman can't breathe! OH NO! I hope this has taught you kids a lesson: kids never learn.

      This is the greatest case of false advertising I've seen since I sued the movie "The Never Ending Story." I'm normally not a praying man, but if you're up there, please save me, Superman. I'm allergic to bee stings. They cause me to, uh, die.

      Whoa, slow down there, maestro. There's a *New* Mexico? What good is money if it can't inspire terror in your fellow man? When I held that gun in my hand, I felt a surge of power…like God must feel when he's holding a gun.
      I don't like being outdoors, Smithers. For one thing, there's too many fat children.
      Bart, with $10,000 we'd be millionaires! We could buy all kinds of useful things like…love! Uh, no, you got the wrong number. This is 9-1…2. Kids, kids. I'm not going to die. That only happens to bad people.

      Shoplifting is a victimless crime. Like punching someone in the dark. Oh, loneliness and cheeseburgers are a dangerous mix. I'm normally not a praying man, but if you're up there, please save me, Superman.

      What good is money if it can't inspire terror in your fellow man?
      Remember the time he ate my goldfish? And you lied and said I never had goldfish. Then why did I have the bowl, Bart? *Why did I have the bowl?* I like my beer cold, my TV loud and my homosexuals flaming.

      Fat Tony is a cancer on this fair city! He is the cancer and I am the…uh…what cures cancer?
      …And the fluffy kitten played with that ball of string all through the night. On a lighter note, a Kwik-E-Mart clerk was brutally murdered last night.
      Here's to alcohol, the cause of — and solution to — all life's problems.
      No children have ever meddled with the Republican Party and lived to tell about it.
      A woman is a lot like a refrigerator. Six feet tall, 300 pounds…it makes ice. Books are useless! I only ever read one book, "To Kill A Mockingbird," and it gave me absolutely no insight on how to kill mockingbirds! Sure it taught me not to judge a man by the color of his skin…but what good does *that* do me?

      Duffman can't breathe! OH NO!
      Bart, with $10,000 we'd be millionaires! We could buy all kinds of useful things like…love!
      I'm allergic to bee stings. They cause me to, uh, die.
      Please do not offer my god a peanut. Yes! I am a citizen! Now which way to the welfare office? I'm kidding, I'm kidding. I work, I work. What's the point of going out? We're just going to wind up back here anyway.

      Save me, Jeebus. A woman is a lot like a refrigerator. Six feet tall, 300 pounds…it makes ice. Mrs. Krabappel and Principal Skinner were in the closet making babies and I saw one of the babies and then the baby looked at me.

      Kids, kids. I'm not going to die. That only happens to bad people. Dear Mr. President, There are too many states nowadays. Please, eliminate three. P.S. I am not a crackpot. I've had it with this school, Skinner. Low test scores, class after class of ugly, ugly children…

      Kids, kids. I'm not going to die. That only happens to bad people. I'm going to the back seat of my car, with the woman I love, and I won't be back for ten minutes! I've done everything the Bible says — even the stuff that contradicts the other stuff!

      What good is money if it can't inspire terror in your fellow man? No children have ever meddled with the Republican Party and lived to tell about it. I didn't get rich by signing checks. Oh, everything looks bad if you remember it.

      Son, when you participate in sporting events, it's not whether you win or lose: it's how drunk you get. Fat Tony is a cancer on this fair city! He is the cancer and I am the…uh…what cures cancer?

      And here I am using my own lungs like a sucker. …And the fluffy kitten played with that ball of string all through the night. On a lighter note, a Kwik-E-Mart clerk was brutally murdered last night.

      Dear Mr. President, There are too many states nowadays. Please, eliminate three. P.S. I am not a crackpot. And now, in the spirit of the season: start shopping. And for every dollar of Krusty merchandise you buy, I will be nice to a sick kid. For legal purposes, sick kids may include hookers with a cold.

      I hope this has taught you kids a lesson: kids never learn. Please do not offer my god a peanut. Lisa, vampires are make-believe, like elves, gremlins, and Eskimos. Duffman can't breathe! OH NO! I hope this has taught you kids a lesson: kids never learn.

      This is the greatest case of false advertising I've seen since I sued the movie "The Never Ending Story." I'm normally not a praying man, but if you're up there, please save me, Superman. I'm allergic to bee stings. They cause me to, uh, die.

Whoa, slow down there, maestro. There's a *New* Mexico? What good is money if it can't inspire terror in your fellow man? When I held that gun in my hand, I felt a surge of power…like God must feel when he's holding a gun.</p>
    </div>
  )
}

export default LongText
