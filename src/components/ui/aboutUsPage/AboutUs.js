import React from 'react'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import Avatar from '@material-ui/core/Avatar'
import Hidden from '@material-ui/core/Hidden'

import history from '../../../assets/history.svg'
import profile from '../../../assets/founder.jpg'
import yearbook from '../../../assets/yearbook.svg'
import puppy from '../../../assets/puppy.svg'

import CTA from '../CTA'

const useStyles = makeStyles(theme => ({
  missionStatement: {
    fontStyle: 'italic',
    fontWeight: 400,
    fontSize: '1.2rem',
    maxWidth: '50em',
    lineHeight: '1.4em',
    marginLeft: '4em',
    marginRight: '4em',
    [theme.breakpoints.down('sm')]: {
      marginLeft: '1.2em',
      marginRight: '1.2em',
    },
  },
  rowContainer: {
    paddingLeft: '4em',
    paddingRight: '4em',
    [theme.breakpoints.down('sm')]: {
      paddingLeft: '1.2em',
      paddingRight: '1.2em',
    }
  },
  avatar: {
    height: '16em',
    width: '16em',
    [theme.breakpoints.down('sm')]: {
      height: '8em',
      width: '8em',
      maxHeight: 300,
      maxWidth: 300,
    }
  },
}))

const AboutUs = (props) => {

  const classes = useStyles()
  const theme = useTheme()
  const matchesMD = useMediaQuery(theme.breakpoints.down('md'))
  const matchesSM = useMediaQuery(theme.breakpoints.down('sm'))
  const matchesXS = useMediaQuery(theme.breakpoints.down('xs'))

  return (
    // Full Page Container wrapping the whole page.
    <Grid container direction='column'>

      {/* Container for Heading and intro text */}
      <Grid item
        className={classes.rowContainer}
        style={{
          marginTop: matchesMD ? '1em' : '3em',
        }}>
        <Typography variant='h1'
          align={matchesMD ? 'center' : undefined}>
          About Us
          </Typography>
      </Grid>

      <Grid item container
        justify='center'
        style={{ marginTop: '3em' }}
        className={classes.rowContainer}>

        <Typography variant='h4'
          align='center'
          className={classes.missionStatement}>
          It doesn't look so shiny to me. Then we'll go with that data file! Daylight and everything.   You won't have time for sleeping, soldier, not with all the bed making you'll be doing. Who said that? SURE you can die! You want to die?!
        </Typography>
      </Grid>

      {/* Container for two side by side containers (Text block and Picture) */}
      <Grid item container
        direction={matchesMD ? 'column' : 'row'}
        alignItems={matchesMD ? 'center' : undefined}
        justify='space-around'
        className={classes.rowContainer}>

        {/* This item is helper for adding lg attribute to child containers */}
        <Grid item>
          {/* Item container for text column block */}
          {/* Containers takes 100% space horzontally */}
          <Grid item container lg
            style={{ maxWidth: '36em' }}
            direction='column'>

            <Grid item>
              <Typography variant='h4'
                style={{ marginTop: '3em' }}
                align={matchesMD ? 'center' : undefined}
                gutterBottom>
                History
              </Typography>
            </Grid>

            <Grid item
              style={{ marginTop: '1em' }}>
              <Typography variant='body1'
                paragraph
                style={{ fontWeight: 700, fontStyle: 'italic' }}>
                We're the new kid on the block...
              </Typography>
              <Typography variant='body1'
                align={matchesMD ? 'center' : undefined}
                paragraph>
                Ok, we'll go deliver this crate like professionals, and then we'll go ride the bumper cars. You know the worst thing about being a slave? They make you work, but they don't pay you or let you go. I had more, but you go ahead.
              </Typography>
              <Typography variant='body1'
                align={matchesMD ? 'center' : undefined}
                paragraph>
                Bender?! You stole the atom. Well, let's just dump it in the sewer and say we delivered it. Yes! In your face, Gandhi! Bender, we're trying our best. I was having the most wonderful dream. Except you were there, and you were there, and you were there!
              </Typography>
              <Typography variant='body1'
                align={matchesMD ? 'center' : undefined}
                paragraph>
                And yet you haven't said what I told you to say! How can any of us trust you? I found what I need. And it's not friends, it's things. Ooh, name it after me! Bender, quit destroying the universe!
              </Typography>
            </Grid>

          </Grid>
        </Grid>

        {/* This item is helper for adding lg attribute to child containers */}
        <Grid item>
          {/* Item container for picture element */}
          <Grid item container lg
            justify='center'
            alignContent='center'>
            <img src={history} alt='quill pen sitting on top of book'
              style={{ marginTop: '8em', maxHeight: matchesMD ? 200 : '20em' }} />
          </Grid>
        </Grid>
      </Grid>

      {/* Team Container starts */}
      <Grid item container direction='column'
        alignItems='center'
        style={{ marginBottom: '8em' }}
        className={classes.rowContainer}>
        <Grid item>
          <Typography variant='h4'
            align='center' gutterBottom>
            Team
          </Typography>
        </Grid>
        <Grid item>
          <Typography variant='body1'
            paragraph align='center'>
            Art Bachmann, Creator
          </Typography>
          <Typography variant='body1'
            paragraph align='center'>
            I started coding when I was already 45 years old...
          </Typography>
        </Grid>
        <Grid>
          <Avatar alt='founder' src={profile}
            className={classes.avatar} />
        </Grid>

        {/* New container holding multiple conainers in the row */}
        <Grid item container
          justify={matchesMD ? 'center' : undefined} >

          {/* Second text column visible when above medium */}
          <Hidden lgUp>
            <Grid item lg
              style={{ maxWidth: '45em', padding: '1.35em' }}>
              <Typography variant='body1'
                align='center' paragraph>
                guess if you want children beaten, you have to do it yourself. Bender! Ship! Stop bickering or I'm going to come back there and change your opinions manually! You're going back for the Countess, aren't you?
            </Typography>
              <Typography variant='body1'
                align='center' paragraph>
                I meant 'physically'. Look, perhaps you could let me work for a little food? I could clean the floors or paint a fence, or service you sexually? Meh. But, like most politicians, he promised more than he could deliver.
            </Typography>
            </Grid>
          </Hidden>

          {/* The first column starts */}
          <Grid item container direction='column'
            style={{ marginBottom: matchesMD ? '2.5em' : 0 }}
            alignItems={matchesMD ? 'center' : undefined} lg>
            <Grid item>
              <img src={yearbook} alt='yearbook page about the founder'
                style={{ maxWidth: matchesMD ? 300 : undefined }} />
            </Grid>
            <Grid item>
              <Typography variant='caption'>
                a page from my Sophomore yearbook
                </Typography>
            </Grid>
          </Grid>

          {/* The second column starts (middle one) */}
          <Hidden mdDown>
            <Grid item lg
              style={{ maxWidth: '45em', padding: '1.35em' }}>
              <Typography variant='body1'
                align='center' paragraph>
                guess if you want children beaten, you have to do it yourself. Bender! Ship! Stop bickering or I'm going to come back there and change your opinions manually! You're going back for the Countess, aren't you?
            </Typography>
              <Typography variant='body1'
                align='center' paragraph>
                I meant 'physically'. Look, perhaps you could let me work for a little food? I could clean the floors or paint a fence, or service you sexually? Meh. But, like most politicians, he promised more than he could deliver.
            </Typography>
            </Grid>
          </Hidden>

          {/* The third column starts (last one) */}
          <Grid item container direction='column' lg
            alignItems={matchesMD ? 'center' : 'flex-end'}>
            <Grid item>
              <img src={puppy} alt='just a nice and sweet puppy'
                style={{ maxWidth: matchesMD ? 300 : undefined }} />
            </Grid>
            <Grid item>
              <Typography variant='caption'>
                hittt iiittt dapp and the whatever...
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <CTA setValue={props.setValue} />
      </Grid>
    </Grid>
  )
}

export default AboutUs
