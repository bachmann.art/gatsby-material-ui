import React from 'react'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import useMediaQuery from '@material-ui/core/useMediaQuery'

import background from '../../assets/background.jpg'
import mobileBackground from '../../assets/mobileBackground.jpg'
import ButtonArrow from './original'

const useStyles = makeStyles(theme => ({
  learnButton: {
    ...theme.typography.learnButton,
    fontSize: '1.1rem',
    fontWeight: 500,
    height: 40,
    paddingRight: 12,
    paddingLeft: 12,
    '&:hover': {
      backgroundColor: theme.palette.common.grey2
    },
  },
  background: {
    backgroundImage: `url(${background})`,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundAttachment: 'fixed',
    height: '60em',
    width: '100%',
    [theme.breakpoints.down('md')]: {
      backgroundImage: `ur(${mobileBackground})`,
      backgroundAttachment: 'inherit'
    }
  },
  estimateButton: {
    ...theme.typography.estimate,
    backgroundColor: theme.palette.common.grey4,
    borderRadius: 50,
    height: 55,
    width: 200,
    marginRight: 190,
    fontSize: '1.4rem',
    fontWeight: 300,
    '&:hover': {
      backgroundColor: theme.palette.secondary.light
    },
    [theme.breakpoints.down('sm')]: {
      marginRight: 0,
      marginLeft: 0,
      marginTop: '2em'
    }
  },
}))

const CTA = () => {
  const classes = useStyles()
  const theme = useTheme()
  const matchesSM = useMediaQuery(theme.breakpoints.down('sm'))

  return (
    <Grid container
      alignItems='center'
      justify={matchesSM ? 'center' : 'space-between'}
      className={classes.background}
      direction={matchesSM ? 'column' : 'row'}>
      <Grid item
        style={{
          marginLeft: matchesSM ? 0 : '5em',
          textAlign: matchesSM ? 'center' : 'inherit'
        }}>
        <Grid container
          direction='column'>
          <Grid item>
            <Typography variant='h3'>
              Simple Software. <br /> Revolutionary Results.
            </Typography>
            <Typography variant='subtitle2'
              style={{ fontSize: '1.4rem', marginBottom: '1.6em' }}>
              Take advantage of the 21st century.
            </Typography>
            <Grid container item
              justify={matchesSM ? 'center' : 'undefined'}>
              <Button
                variant='outlined'
                className={classes.learnButton}
              >
                <span style={{ marginRight: 10 }}>Learn More</span>
                <ButtonArrow
                  width={15}
                  height={15}
                  fill={theme.palette.common.gray4}
                />
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <Button
          className={classes.estimateButton}
          variant='contained'>Free Estimate</Button>
      </Grid>
    </Grid>
  )
}

export default CTA
