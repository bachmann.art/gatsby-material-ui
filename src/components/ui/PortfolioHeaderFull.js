import React from 'react'
import Grid from '@material-ui/core/Grid'
import { makeStyles } from '@material-ui/core/styles'

import ResumePortfolioHeaderFull from '../../assets/myAssets/HeroBackgroundMain.png'
import Header from './Header'

const useStyles = makeStyles(theme => ({
  ResumePortfolioHeaderFull: {
    backgroundImage: `url(${ResumePortfolioHeaderFull})`,
    position: 'sticky',
    display: 'flex',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    height: '100%',
    width: '100%',
    marginBottom: '1vh',
    maxHeight: '180px',
  }
}))

const PortfolioHeaderFull = () => {
  const classes = useStyles()
  return (
    <Grid container direction='column'>
      <Grid item style={{ with: '100vw' }} stickyHeader={true}>
        <img
          className={classes.ResumePortfolioHeaderFull}
          alt='portfolio main header background'
          src={ResumePortfolioHeaderFull}
        />
      </Grid>
    </Grid>
  )
}

export default PortfolioHeaderFull
