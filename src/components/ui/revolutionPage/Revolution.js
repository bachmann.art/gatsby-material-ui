import React from 'react'
import Lottie from 'react-lottie'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import useMediaQuery from '@material-ui/core/useMediaQuery'

import vision from '../../../assets/vision.svg'
import consultation from '../../../assets/consultationIcon.svg'
import mockup from '../../../assets/mockupIcon.svg'
import review from '../../../assets/reviewIcon.svg'
import design from '../../../assets/designIcon.svg'
import build from '../../../assets/buildIcon.svg'
import launch from '../../../assets/launchIcon.svg'
import maintain from '../../../assets/maintainIcon.svg'
import iterate from '../../../assets/iterateIcon.svg'



import technologyAnimation from '../../../animations/technologyAnimation/data.json'

const useStyles = makeStyles(theme => ({
  rowContainer: {
    paddingLeft: '4em',
    paddingRight: '4em',
    paddingTop: '4em',
    paddingBottom: '2em',
    [theme.breakpoints.down('sm')]: {
      paddingLeft: '1.2em',
      paddingRight: '1.2em',
      paddingBottom: '1em',
      paddingTop: '1em'
    }
  },
}))

const Revolution = () => {

  const classes = useStyles()
  const theme = useTheme()
  const matchesMD = useMediaQuery(theme.breakpoints.down('md'))
  const matchesSM = useMediaQuery(theme.breakpoints.down('sm'))
  const matchesXS = useMediaQuery(theme.breakpoints.down('xs'))

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: technologyAnimation,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  }


  return (
    <Grid container
      direction='column'>

      {/* The Revolution Heading Section */}
      <Grid item
        className={classes.rowContainer}>
        <Typography variant='h1'
          align={matchesMD ? 'center' : undefined}>
          The Revolution
        </Typography>
      </Grid>

      {/* Firs Section (vision) of Revolution Page */}
      <Grid item container
        alignItems='center'
        direction={matchesMD ? 'column' : 'row'}
        className={classes.rowContainer}>

        <Grid item lg>
          <img src={vision} alt='mountain through binoculars'
            style={{
              maxWidth: matchesSM ? 300 : '40em',
              marginRight: matchesMD ? 0 : '3em',
              marginBottom: matchesMD ? '5em' : 0
            }} />
        </Grid>
        <Grid item container lg
          style={{ marginLeft: '3em', maxWidth: '40em' }}
          direction='column'>
          <Grid item>
            <Typography variant='h4'
              align='right'
              gutterBottom>
              Vision
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant='body1'
              align='right'
              paragraph>
              All I want is to be a monkey of moderate intelligence who wears a suit… that's why I'm transferring to business school! No argument here. We don't have a brig. Hello Morbo, how's the family?
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant='body1'
              align='right'
              paragraph>
              No, she'll probably make me do it. Who are you, my warranty?! Noooooo! Your best is an idiot! Why am I sticky and naked? Did I miss something fun? Ah, the 'Breakfast Club' soundtrack! I can't wait til I'm old enough to feel ways about stuff!
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant='body1'
              align='right'
              paragraph>
              Um, is this the boring, peaceful kind of taking to the streets? Oh, I think we should just stay friends. Meh. Who's brave enough to fly into something we all keep calling a death sphere? You can crush me but you can't crush my spirit!
            </Typography>
          </Grid>
        </Grid>
      </Grid>

      {/* Second Section (Technology) of Revolution Page */}
      <Grid item container
        alignItems='center'
        direction={matchesMD ? 'column' : 'row'}
        className={classes.rowContainer}>
        <Grid item container lg
          style={{ marginLeft: '3em', maxWidth: '40em' }}
          direction='column'>
          <Grid item>
            <Typography variant='h4'
              gutterBottom>
              Technology
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant='body1'
              paragraph>
              All I want is to be a monkey of moderate intelligence who wears a suit… that's why I'm transferring to business school! No argument here. We don't have a brig. Hello Morbo, how's the family?
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant='body1'
              paragraph>
              No, she'll probably make me do it. Who are you, my warranty?! Noooooo! Your best is an idiot! Why am I sticky and naked? Did I miss something fun? Ah, the 'Breakfast Club' soundtrack! I can't wait til I'm old enough to feel ways about stuff!
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant='body1'
              paragraph>
              Um, is this the boring, peaceful kind of taking to the streets? Oh, I think we should just stay friends. Meh. Who's brave enough to fly into something we all keep calling a death sphere? You can crush me but you can't crush my spirit!
            </Typography>
          </Grid>
        </Grid>
        <Grid item container
          justify={matchesMD ? 'center' : 'flex-end'} lg>
          <Lottie options={defaultOptions}
            style={{ maxWidth: '22em', margin: 0 }}
            isStopped='true' />
        </Grid>
      </Grid>

      {/* Process Sections Start */}
      <Grid item container
        className={classes.rowContainer}
        direction='row'
        justify='center'>
        <Grid item>
          <Typography variant='h4'
            gutterBottom>
            Process
          </Typography>
        </Grid>
      </Grid>

      {/* Consultation Section */}
      <Grid item container
        className={classes.rowContainer}
        style={{ background: '#a0afb2', height: '60em' }}
        direction='row'
        justify='center'>
        <Grid item container lg
          direction='column'>
          <Grid item>
            <Typography variant='h4'
              style={{ color: '#000', }}
              gutterBottom>
              Consultation
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant='body1'
              style={{ color: '#fff', maxWidth: '22em' }}
              paragraph>
              Soon enough. And from now on you're all named Bender Jr. Yes! In your face, Gandhi! You're going to do his laundry? Good news, everyone! There's a report on TV with some very bad news!
            </Typography>
            <Typography variant='body1'
              style={{ color: '#fff', maxWidth: '22em' }}
              paragraph>
              No! The kind with looting and maybe starting a few fires! Goodbye, cruel world. Goodbye, cruel lamp. Goodbye, cruel velvet drapes, lined with what would appear to be some sort of cruel muslin and the cute little pom-pom curtain pull cords. Cruel though they may be…
            </Typography>
            <Typography variant='body1'
              style={{ color: '#fff', maxWidth: '22em' }}
              paragraph>
              It may comfort you to know that Fry's death took only fifteen seconds, yet the pain was so intense, that it felt to him like fifteen years. And it goes without saying, it caused him to empty his bowels. Aww, it's true. I've been hiding it for so long.
            </Typography>
          </Grid>
        </Grid>
        <Grid item lg
          style={{ alignSelf: 'center' }}>
          <img src={consultation}
            alt='handshake'
            style={{ maxWidth: '36em', maxHeight: '36em' }} />
        </Grid>
      </Grid>

      {/* Mockup Section */}
      <Grid item container
        className={classes.rowContainer}
        style={{ background: '#009688', height: '60em' }}
        direction='row'
        justify='center'>
        <Grid item container lg
          direction='column'>
          <Grid item>
            <Typography variant='h4'
              style={{ color: '#000', }}
              gutterBottom>
              Mockup
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant='body1'
              style={{ color: '#fff', maxWidth: '22em' }}
              paragraph>
              Soon enough. And from now on you're all named Bender Jr. Yes! In your face, Gandhi! You're going to do his laundry? Good news, everyone! There's a report on TV with some very bad news!
            </Typography>
            <Typography variant='body1'
              style={{ color: '#fff', maxWidth: '22em' }}
              paragraph>
              No! The kind with looting and maybe starting a few fires! Goodbye, cruel world. Goodbye, cruel lamp. Goodbye, cruel velvet drapes, lined with what would appear to be some sort of cruel muslin and the cute little pom-pom curtain pull cords. Cruel though they may be…
            </Typography>
            <Typography variant='body1'
              style={{ color: '#fff', maxWidth: '22em' }}
              paragraph>
              It may comfort you to know that Fry's death took only fifteen seconds, yet the pain was so intense, that it felt to him like fifteen years. And it goes without saying, it caused him to empty his bowels. Aww, it's true. I've been hiding it for so long.
            </Typography>
          </Grid>
        </Grid>
        <Grid item lg
          style={{ alignSelf: 'center' }}>
          <img src={mockup}
            alt='mockup image'
            style={{ maxWidth: '36em', maxHeight: '36em' }} />
        </Grid>
      </Grid>

      {/* Review Section */}
      <Grid item container
        className={classes.rowContainer}
        style={{ background: '#455A64', height: '60em' }}
        direction='row'
        justify='center'>
        <Grid item container lg
          direction='column'>
          <Grid item>
            <Typography variant='h4'
              style={{ color: '#000', }}
              gutterBottom>
              Review
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant='body1'
              style={{ color: '#fff', maxWidth: '22em' }}
              paragraph>
              Soon enough. And from now on you're all named Bender Jr. Yes! In your face, Gandhi! You're going to do his laundry? Good news, everyone! There's a report on TV with some very bad news!
            </Typography>
            <Typography variant='body1'
              style={{ color: '#fff', maxWidth: '22em' }}
              paragraph>
              No! The kind with looting and maybe starting a few fires! Goodbye, cruel world. Goodbye, cruel lamp. Goodbye, cruel velvet drapes, lined with what would appear to be some sort of cruel muslin and the cute little pom-pom curtain pull cords. Cruel though they may be…
            </Typography>
            <Typography variant='body1'
              style={{ color: '#fff', maxWidth: '22em' }}
              paragraph>
              It may comfort you to know that Fry's death took only fifteen seconds, yet the pain was so intense, that it felt to him like fifteen years. And it goes without saying, it caused him to empty his bowels. Aww, it's true. I've been hiding it for so long.
            </Typography>
          </Grid>
        </Grid>
        <Grid item lg
          style={{ alignSelf: 'center' }}>
          <img src={review}
            alt='review image'
            style={{ maxWidth: '36em', maxHeight: '36em' }} />
        </Grid>
      </Grid>

      {/* Design Section */}
      <Grid item container
        className={classes.rowContainer}
        style={{ background: '#512DA8', height: '60em' }}
        direction='row'
        justify='center'>
        <Grid item container lg
          direction='column'>
          <Grid item>
            <Typography variant='h4'
              style={{ color: '#000', }}
              gutterBottom>
              Design
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant='body1'
              style={{ color: '#fff', maxWidth: '22em' }}
              paragraph>
              Soon enough. And from now on you're all named Bender Jr. Yes! In your face, Gandhi! You're going to do his laundry? Good news, everyone! There's a report on TV with some very bad news!
            </Typography>
            <Typography variant='body1'
              style={{ color: '#fff', maxWidth: '22em' }}
              paragraph>
              No! The kind with looting and maybe starting a few fires! Goodbye, cruel world. Goodbye, cruel lamp. Goodbye, cruel velvet drapes, lined with what would appear to be some sort of cruel muslin and the cute little pom-pom curtain pull cords. Cruel though they may be…
            </Typography>
            <Typography variant='body1'
              style={{ color: '#fff', maxWidth: '22em' }}
              paragraph>
              It may comfort you to know that Fry's death took only fifteen seconds, yet the pain was so intense, that it felt to him like fifteen years. And it goes without saying, it caused him to empty his bowels. Aww, it's true. I've been hiding it for so long.
            </Typography>
          </Grid>
        </Grid>
        <Grid item lg
          style={{ alignSelf: 'center' }}>
          <img src={design}
            alt='design image'
            style={{ maxWidth: '36em', maxHeight: '36em' }} />
        </Grid>
      </Grid>

      {/* Build Section */}
      <Grid item container
        className={classes.rowContainer}
        style={{ background: '#FF9800', height: '60em' }}
        direction='row'
        justify='center'>
        <Grid item container lg
          direction='column'>
          <Grid item>
            <Typography variant='h4'
              style={{ color: '#000', }}
              gutterBottom>
              Build
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant='body1'
              style={{ color: '#000', maxWidth: '22em' }}
              paragraph>
              Soon enough. And from now on you're all named Bender Jr. Yes! In your face, Gandhi! You're going to do his laundry? Good news, everyone! There's a report on TV with some very bad news!
            </Typography>
            <Typography variant='body1'
              style={{ color: '#000', maxWidth: '22em' }}
              paragraph>
              No! The kind with looting and maybe starting a few fires! Goodbye, cruel world. Goodbye, cruel lamp. Goodbye, cruel velvet drapes, lined with what would appear to be some sort of cruel muslin and the cute little pom-pom curtain pull cords. Cruel though they may be…
            </Typography>
            <Typography variant='body1'
              style={{ color: '#000', maxWidth: '22em' }}
              paragraph>
              It may comfort you to know that Fry's death took only fifteen seconds, yet the pain was so intense, that it felt to him like fifteen years. And it goes without saying, it caused him to empty his bowels. Aww, it's true. I've been hiding it for so long.
            </Typography>
          </Grid>
        </Grid>
        <Grid item lg
          style={{ alignSelf: 'center' }}>
          <img src={build}
            alt='build image'
            style={{ maxWidth: '36em', maxHeight: '36em' }} />
        </Grid>
      </Grid>

      {/* Launch Section */}
      <Grid item container
        className={classes.rowContainer}
        style={{ background: '#CDDC39', height: '60em' }}
        direction='row'
        justify='center'>
        <Grid item container lg
          direction='column'>
          <Grid item>
            <Typography variant='h4'
              style={{ color: '#000', }}
              gutterBottom>
              Launch
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant='body1'
              style={{ color: '#000', maxWidth: '22em' }}
              paragraph>
              Soon enough. And from now on you're all named Bender Jr. Yes! In your face, Gandhi! You're going to do his laundry? Good news, everyone! There's a report on TV with some very bad news!
            </Typography>
            <Typography variant='body1'
              style={{ color: '#000', maxWidth: '22em' }}
              paragraph>
              No! The kind with looting and maybe starting a few fires! Goodbye, cruel world. Goodbye, cruel lamp. Goodbye, cruel velvet drapes, lined with what would appear to be some sort of cruel muslin and the cute little pom-pom curtain pull cords. Cruel though they may be…
            </Typography>
            <Typography variant='body1'
              style={{ color: '#000', maxWidth: '22em' }}
              paragraph>
              It may comfort you to know that Fry's death took only fifteen seconds, yet the pain was so intense, that it felt to him like fifteen years. And it goes without saying, it caused him to empty his bowels. Aww, it's true. I've been hiding it for so long.
            </Typography>
          </Grid>
        </Grid>
        <Grid item lg
          style={{ alignSelf: 'center' }}>
          <img src={launch}
            alt='launch image'
            style={{ maxWidth: '36em', maxHeight: '36em' }} />
        </Grid>
      </Grid>

      {/* Maintain Section */}
      <Grid item container
        className={classes.rowContainer}
        style={{ background: '#607D8B', height: '60em' }}
        direction='row'
        justify='center'>
        <Grid item container lg
          direction='column'>
          <Grid item>
            <Typography variant='h4'
              style={{ color: '#000', }}
              gutterBottom>
              Maintain
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant='body1'
              style={{ color: '#fff', maxWidth: '22em' }}
              paragraph>
              Soon enough. And from now on you're all named Bender Jr. Yes! In your face, Gandhi! You're going to do his laundry? Good news, everyone! There's a report on TV with some very bad news!
            </Typography>
            <Typography variant='body1'
              style={{ color: '#fff', maxWidth: '22em' }}
              paragraph>
              No! The kind with looting and maybe starting a few fires! Goodbye, cruel world. Goodbye, cruel lamp. Goodbye, cruel velvet drapes, lined with what would appear to be some sort of cruel muslin and the cute little pom-pom curtain pull cords. Cruel though they may be…
            </Typography>
            <Typography variant='body1'
              style={{ color: '#fff', maxWidth: '22em' }}
              paragraph>
              It may comfort you to know that Fry's death took only fifteen seconds, yet the pain was so intense, that it felt to him like fifteen years. And it goes without saying, it caused him to empty his bowels. Aww, it's true. I've been hiding it for so long.
            </Typography>
          </Grid>
        </Grid>
        <Grid item lg
          style={{ alignSelf: 'center' }}>
          <img src={maintain}
            alt='maintain image'
            style={{ maxWidth: '36em', maxHeight: '36em' }} />
        </Grid>
      </Grid>

      {/* Iterate Section */}
      <Grid item container
        className={classes.rowContainer}
        style={{ background: '#303F9F', height: '60em' }}
        direction='row'
        justify='center'>
        <Grid item container lg
          direction='column'>
          <Grid item>
            <Typography variant='h4'
              style={{ color: '#000', }}
              gutterBottom>
              Iterate
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant='body1'
              style={{ color: '#fff', maxWidth: '22em' }}
              paragraph>
              Soon enough. And from now on you're all named Bender Jr. Yes! In your face, Gandhi! You're going to do his laundry? Good news, everyone! There's a report on TV with some very bad news!
            </Typography>
            <Typography variant='body1'
              style={{ color: '#fff', maxWidth: '22em' }}
              paragraph>
              No! The kind with looting and maybe starting a few fires! Goodbye, cruel world. Goodbye, cruel lamp. Goodbye, cruel velvet drapes, lined with what would appear to be some sort of cruel muslin and the cute little pom-pom curtain pull cords. Cruel though they may be…
            </Typography>
            <Typography variant='body1'
              style={{ color: '#fff', maxWidth: '22em' }}
              paragraph>
              It may comfort you to know that Fry's death took only fifteen seconds, yet the pain was so intense, that it felt to him like fifteen years. And it goes without saying, it caused him to empty his bowels. Aww, it's true. I've been hiding it for so long.
            </Typography>
          </Grid>
        </Grid>
        <Grid item lg
          style={{ alignSelf: 'center' }}>
          <img src={iterate}
            alt='iterate'
            style={{ maxWidth: '36em', maxHeight: '36em' }} />
        </Grid>
      </Grid>
    </Grid>
  )
}

export default Revolution
