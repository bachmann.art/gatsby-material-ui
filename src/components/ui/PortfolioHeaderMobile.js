import React from 'react'
import Grid from '@material-ui/core/Grid'
import { makeStyles } from '@material-ui/core/styles'

import ResumePortfolioHeaderMobile from '../../assets/myAssets/HeroBackgroundMobile.png'


const useStyles = makeStyles(theme => ({
  ResumePortfolioHeaderMobile: {
    backgroundImage: `url(${ResumePortfolioHeaderMobile})`,
    position: 'relative',
    display: 'block',
    backgroundPosition: 'center',
    backgroundSize: 'auto',
    backgroundRepeat: 'repeat-x',
    height: '120px',
    width: '100%',
  }
}))

const PortfolioHeaderMobile = () => {
  const classes = useStyles()
  return (
    <Grid container direction='column'>
      <Grid item style={{ with: '100vw' }}>
        <img
          className={classes.ResumePortfolioHeaderMobile}
          alt='portfolio main header background'
          src={ResumePortfolioHeaderMobile}
        />
      </Grid>
    </Grid>
  )
}

export default PortfolioHeaderMobile
