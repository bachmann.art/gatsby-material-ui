import React from 'react'
import { Link } from 'gatsby'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import IconButton from '@material-ui/core/IconButton'
import Hidden from '@material-ui/core/Hidden'

import backArrow from '../../../assets/backArrow.svg'
import forwardArrow from '../../../assets/forwardArrow.svg'
import analytics from '../../../assets/analytics.svg'
import seo from '../../../assets/seo.svg'
import outreach from '../../../assets/outreach.svg'
import ecommerce from '../../../assets/ecommerce.svg'

import CTA from '../CTA'

const useStyles = makeStyles(theme => ({
  rowContainer: {
    paddingLeft: '4em',
    paddingRight: '4em',
    paddingTop: '2em',
    paddingBottom: '2em',
    [theme.breakpoints.down('sm')]: {
      paddingLeft: '1.2em',
      paddingRight: '1.2em',
      paddingBottom: '1em',
      paddingTop: '1em'
    }
  },
  heading: {
    maxWidth: '40em',
  },
  arrowContainer: {
    marginTop: '0.5em'
  },
  itemContainer: {
    maxWidth: '40em'
  },
  paragraphContainer: {
    maxWidth: '30em'
  },
  // introContainer: {
  //   marginTop: '3em',
  //   [theme.breakpoints.down('sm')]: {
  //     marginTop: '1em'
  //   }
  // }
}))

const WebDevelopment = (props) => {

  const classes = useStyles()
  const theme = useTheme()
  const matchesMD = useMediaQuery(theme.breakpoints.down('md'))
  const matchesSM = useMediaQuery(theme.breakpoints.down('sm'))
  const matchesXS = useMediaQuery(theme.breakpoints.down('xs'))

  return (
    <Grid container direction='column'>
      {/* Container Wrapping first section with heading, body and arrows to navigate */}
      <Grid item container
        direction='row'
        className={classes.introContainer}
        style={{ marginTop: matchesSM ? 0 : '3em' }}
        // if on the medium breakpoint or below!! center if on larger >> stays where is..
        justify={matchesMD ? 'center' : undefined}>
        {/* Arrow button back */}
        <Hidden smDown>
          <Grid item
            className={classes.arrowContainer}
            style={{ marginRight: '1em', marginLeft: '-3.5em' }}>
            <Link to='/ios_android/'>
              <IconButton style={{ background: 'transparent' }}>
                <img src={backArrow} alt='Back to Mobile App Development Page' />
              </IconButton>
            </Link>
          </Grid>
        </Hidden>

        {/* Container holding heading and body items */}
        <Grid item container
          direction='column'
          style={{ marginLeft: '2em' }}
          className={classes.heading}>
          <Grid item>
            <Typography variant='h1'
              align={matchesMD ? 'center' : undefined}
              style={{ marginBottom: '1em' }}>
              Website Development
            </Typography>
          </Grid>

          <Grid item>
            <Typography
              variant='body1'
              paragraph>
              …And the fluffy kitten played with that ball of string all through the night. On a lighter note, a Kwik-E-Mart clerk was brutally murdered last night. Last night's "Itchy and Scratchy Show" was, without a doubt, the worst episode *ever.* Rest assured, I was on the Internet within minutes, registering my disgust throughout the world.
            </Typography>
          </Grid>

          <Grid item>
            <Typography
              variant='body1'
              paragraph>
              He didn't give you gay, did he? Did he?! Kids, we need to talk for a moment about Krusty Brand Chew Goo Gum Like Substance. We all knew it contained spider eggs, but the hantavirus? That came out of left field. So if you're experiencing numbness and/or comas, send five dollars to antidote, PO box…
            </Typography>
          </Grid>

          <Grid item>
            <Typography
              variant='body1'
              paragraph>
              I was saying "Boo-urns. " I'm allergic to bee stings. They cause me to, uh, die. D'oh. Shoplifting is a victimless crime. Like punching someone in the dark. Please do not offer my god a peanut.
            </Typography>
          </Grid>
        </Grid>

        {/* Forward arrow */}
        <Hidden smDown>
          <Link to='/services/'>
            <Grid item className={classes.arrowContainer}>
              <IconButton style={{ background: 'transparent' }}>
                <img src={forwardArrow} alt='Services Page' />
              </IconButton>
            </Grid>
          </Link>
        </Hidden>
      </Grid>

      {/* Analytics section */}
      <Grid item container
        direction={matchesSM ? 'column' : 'row'}
        // Since the direction is row then items are centered y axis direction up-down
        alignItems='center'
        style={{ marginTop: '7em' }}
        className={classes.rowContainer}>
        <Grid item>
          <Grid container
            direction='column'>
            <Grid item>
              <Typography variant='h4'
                align={matchesSM ? 'center' : undefined}
                gutterBottom>
                Analytics
                </Typography>
            </Grid>
            <Grid item>
              <img src={analytics}
                style={{ marginLeft: '-2.8em' }}
                alt='graph with magnifying glass...' />
            </Grid>
          </Grid>
        </Grid>
        <Grid item
          style={{ marginLeft: matchesSM ? '0' : '2em' }}
          className={classes.paragraphContainer}>
          <Typography variant='body1'
            align={matchesSM ? 'center' : undefined}>
            I was saying "Boo-urns. " I'm allergic to bee stings.
            They cause me to, uh, die. D'oh. Shoplifting is a victimless crime.
            Like punching someone in the dark. Please do not offer my god a peanut.
          </Typography>
        </Grid>
      </Grid>

      {/* E-commerce section */}
      <Grid item container
        direction={matchesSM ? 'column' : 'row'}
        // Since the direction is row then items are centered y axis direction up-down
        alignItems='center'
        justify='flex-end'
        style={{ marginTop: '4em', marginBottom: '4em' }}
        className={classes.rowContainer}>
        <Grid item>
          <Grid container
            direction='column'>
            <Grid item>
              <Typography align='center' variant='h4'
                gutterBottom>
                E-Commerce
                </Typography>
            </Grid>
            <Grid item>
              <img src={ecommerce}
                alt='world outline made of dollar signs...' />
            </Grid>
          </Grid>
        </Grid>
        <Grid item style={{ marginLeft: matchesSM ? '0' : '2em' }}
          className={classes.paragraphContainer}>
          <Typography variant='body1'
            align={matchesSM ? 'center' : undefined}
            paragraph>
            Marge, you being a cop makes you the man! Which makes me the
            woman — and I have no interest in that, besides occasionally
            wearing the underwear, which as we discussed, is strictly a
            comfort thing.
          </Typography>
          <Typography variant='body1'
            align={matchesSM ? 'center' : undefined}
            paragraph>
            Aaah! Natural light! Get it off me! Get it off me!
          </Typography>
        </Grid>
      </Grid>

      {/* SEO section */}
      <Grid item container
        direction={matchesSM ? 'column' : 'row'}
        alignItems='center'
        className={classes.rowContainer}>
        <Grid item>
          <Grid container
            direction='column'>
            <Grid item>
              <Typography variant='h4'
                gutterBottom>
                Search Engine <br />
                Optimization
                </Typography>
            </Grid>
            <Grid item>
              <img src={seo}
                style={{ marginLeft: '-2.8em' }}
                alt='website standing on winners podium...' />
            </Grid>
          </Grid>
        </Grid>
        <Grid item
          style={{ marginLeft: matchesSM ? '0' : '2em' }}
          className={classes.paragraphContainer}>
          <Typography variant='body1'
            align={matchesSM ? 'center' : undefined}>
            I was saying "Boo-urns. " I'm allergic to bee stings.
            They cause me to, uh, die. D'oh. Shoplifting is a victimless crime.
            Like punching someone in the dark. Please do not offer my god a peanut.
          </Typography>
        </Grid>
      </Grid>

      {/* Outreach section  */}
      <Grid item container
        direction={matchesSM ? 'column' : 'row'}
        alignItems='center'
        justify='flex-end'
        style={{ marginBottom: '4em', marginTop: '4em' }}
        className={classes.rowContainer}>
        <Grid item>
          <Grid container
            direction='column'>
            <Grid item>
              <Typography align='center' variant='h4'
                gutterBottom>
                Outreach
                </Typography>
            </Grid>
            <Grid item>
              <img src={outreach}
                alt='world outline made of dollar signs...' />
            </Grid>
          </Grid>
        </Grid>
        <Grid item style={{ marginLeft: matchesSM ? '0' : '2em' }}
          className={classes.paragraphContainer}>
          <Typography variant='body1'
            align={matchesSM ? 'center' : undefined}
            paragraph>
            Marge, you being a cop makes you the man! Which makes me the
            woman — and I have no interest in that, besides occasionally
            wearing the underwear, which as we discussed, is strictly a
            comfort thing.
          </Typography>
          <Typography variant='body1'
            align={matchesSM ? 'center' : undefined}
            paragraph>
            Aaah! Natural light! Get it off me! Get it off me!
          </Typography>
        </Grid>
      </Grid>
      <CTA setValue={props.setValue} />
    </Grid>
  )
}

export default WebDevelopment
