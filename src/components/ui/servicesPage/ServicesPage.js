import React from 'react'
import { Link } from 'gatsby'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import ButtonArrow from '../original'
import Typography from '@material-ui/core/Typography'
import useMediaQuery from '@material-ui/core/useMediaQuery'

import customSoftwareIcon from '../../../assets/Custom Software Icon.svg'
import websitesIcon from '../../../assets/websiteIcon.svg'
import mobileAppIcon from '../../../assets/mobileIcon.svg'

const useStyles = makeStyles(theme => ({
  mainContainer: {
    marginTop: '2em',
    [theme.breakpoints.down('md')]: {
      marginLeft: '3em',
      marginRight: '3em'
    },
    [theme.breakpoints.down('xs')]: {
      marginLeft: '2em',
      marginRight: '2em'
    },
  },
  specialText: {
    fontFamily: 'Teko',
    fontSize: '1.4rem',
    fontWeight: '300',
    color: theme.palette.common.violet1
  },
  learnButton: {
    ...theme.typography.learnButton,
    fontSize: '1.1rem',
    fontWeight: 500,
    height: 40,
    paddingRight: 12,
    paddingLeft: 12,
    marginTop: '1em',
    '&:hover': {
      backgroundColor: theme.palette.common.grey1
    },
    [theme.breakpoints.down('sm')]: {
      marginBottom: '2em',
      marginTop: '1.4rem'
    },
    textDecorationColor: 'white'
  },
  subtitle: {
    marginBottom: '1em',
    marginTop: '0.6em'
  },
  icon: {
    marginLeft: '2em',
    marginRight: '2em',
    [theme.breakpoints.down('xs')]: {
      marginLeft: 0,
      marginRight: 0
    }
  },
  serviceContainer: {
    marginTop: '12em',
    [theme.breakpoints.down('md')]: {
      marginTop: '7em'
    },
    [theme.breakpoints.down('sm')]: {
      marginTop: '5em'
    },
    [theme.breakpoints.down('xs')]: {
      marginTop: '2em'
    },
  }
}))

const Services = () => {
  const classes = useStyles()
  const theme = useTheme()
  const matchesSM = useMediaQuery(theme.breakpoints.down('sm'))

  return (
    <Grid container
      direction='column'
      className={classes.mainContainer}>
      <Grid item>
        <Typography
          align={matchesSM ? 'center' : undefined}
          style={{ marginLeft: matchesSM ? 0 : '5em' }}
          variant='h1'
          gutterBottom>
          Services
        </Typography>
      </Grid>
      <Grid item>

        {/* IOS/Android App Development */}
        <Grid container
          direction='row'
          className={classes.serviceContainer}
          justify={matchesSM ? 'center' : 'flex-end'}
          style={{ marginTop: matchesSM ? '1em' : '8em' }}>
          <Grid item
            style={{
              textAlign: matchesSM ? 'center' : undefined,
              width: matchesSM ? undefined : '35em'
            }} >
            <Typography variant='h4'>
              IOS/Android App Development
              </Typography>
            <Typography variant='subtitle1' className={classes.subtitle}>
              Extend functionality. Extend Access. Increase Engagement.
            </Typography>
            <Typography variant='subtitle1'>
              Integrate your web experience or create standalone {' '}

              application{matchesSM ? null : <br />} with either  <span className={classes.specialText}>mobile platform.</span>
            </Typography>
            <Link to='/ios_android'>
              <Button variant='outlined'
                className={classes.learnButton}>
                <span style={{ marginRight: 10, textDecorationColor: 'red' }}>Learn More</span>
                <ButtonArrow width={15} height={15}
                  fill={theme.palette.common.grey4} />
              </Button>
            </Link>
          </Grid>
          <Grid item
            style={{ marginRight: matchesSM ? 0 : '5em' }}>
            <img
              className={classes.icon}
              alt='mobile phone icon'
              src={mobileAppIcon}
              width='250em'
            />
          </Grid>
        </Grid>

        {/* Custom Software Development */}
        <Grid container
          direction='row'
          className={classes.serviceContainer}
          justify={matchesSM ? 'center' : undefined}>

          <Grid item
            style={{
              marginLeft: matchesSM ? 0 : '5em',
              textAlign: matchesSM ? 'center' : undefined
            }}>

            <Typography variant='h4'>
              Custom Software Development
              </Typography>
            <Typography variant='subtitle1' className={classes.subtitle}>
              Save Energy. Save Time. Save Money.
            </Typography>
            <Typography variant='subtitle1'>
              Complete digital solutions, from investigation to {' '}
              <span className={classes.specialText}>celebration.</span>
            </Typography>
            <Link to='/customsoftware/'>
              <Button variant='outlined'
                className={classes.learnButton}>
                <span style={{ marginRight: 10 }}>Learn More</span>
                <ButtonArrow width={15} height={15}
                  fill={theme.palette.common.grey4} />
              </Button>
            </Link>
          </Grid>
          <Grid item>
            <img
              className={classes.icon}
              alt='custom software icon'
              src={customSoftwareIcon}
              width='250em'
            />
          </Grid>
        </Grid>

        {/* Website Development */}
        <Grid container
          direction='row'
          className={classes.serviceContainer}
          justify={matchesSM ? 'center' : 'flex-end'}>
          <Grid item
            style={{
              textAlign: matchesSM ? 'center' : undefined,
              width: matchesSM ? undefined : '35em'
            }}>

            <Typography variant='h4'>
              Website Development
              </Typography>
            <Typography variant='subtitle1' className={classes.subtitle}>
              Reach More. Discover More. Sell More.
            </Typography>
            <Typography variant='subtitle1'>
              Optimize for search engines. Built for {' '}
              <span className={classes.specialText}>speed.</span>
            </Typography>
            <Link to='/web_development/'>
              <Button variant='outlined'
                className={classes.learnButton}>
                <span style={{ marginRight: 10 }}>Learn More</span>
                <ButtonArrow width={15} height={15}
                  fill={theme.palette.common.grey4} />
              </Button>
            </Link>
          </Grid>
          <Grid item style={{ marginRight: matchesSM ? 0 : '5em' }}>
            <img
              className={classes.icon}
              alt='custom websit icon'
              src={websitesIcon}
              width='250em'
            />
          </Grid>
        </Grid>
      </Grid>
    </Grid >
  )
}

export default Services

