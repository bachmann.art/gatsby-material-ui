import React from 'react'
import Lottie from 'react-lottie'
import { Link } from 'gatsby'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import IconButton from '@material-ui/core/IconButton'
import Hidden from '@material-ui/core/Hidden'

import scaleAnimation from '../../../animations/scaleAnimation/data.json'
import documentsAnimation from '../../../animations/documentsAnimation/data'
import automationAnimation from '../../../animations/automationAnimation/data.json'
import uxAnimation from '../../../animations/uxAnimation/data'


import backArrow from '../../../assets/backArrow.svg'
import forwardArrow from '../../../assets/forwardArrow.svg'
import lightbulb from '../../../assets/bulb.svg'
import cash from '../../../assets/cash.svg'
import stopwatch from '../../../assets/stopwatch.svg'
import roots from '../../../assets/root.svg'

import CTA from '../../../components/ui/CTA'


const useStyles = makeStyles(theme => ({
  mainContainer: {
    paddingLeft: '4em',
    paddingRight: '4em',
    paddingTop: '2em',
    paddingBottom: '7em',
    [theme.breakpoints.down('sm')]: {
      paddingLeft: '1.2em',
      paddingRight: '1.2em',
      paddingBottom: '1em',
      paddingTop: '1em'
    }
  },
  heading: {
    maxWidth: '40em',
  },
  arrowContainer: {
    marginTop: '0.5em'
  },
  itemContainer: {
    maxWidth: '40em'
  }
}))

const CustomSoftware = (props) => {
  const classes = useStyles()
  const theme = useTheme()
  const matchesMD = useMediaQuery(theme.breakpoints.down('md'))
  const matchesSM = useMediaQuery(theme.breakpoints.down('sm'))

  const documentsOptions = {
    loop: true,
    autoplay: false,
    animationData: documentsAnimation,
    renderSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  }

  const scaleOptions = {
    loop: true,
    autoplay: true,
    animationData: scaleAnimation,
    renderSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  }

  const automationOptions = {
    loop: true,
    autoplay: true,
    animationData: automationAnimation,
    renderSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  }

  const uxOptions = {
    loop: true,
    autoplay: true,
    animationData: uxAnimation,
    renderSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  }

  return (
    // Container wrapping grid to entire page
    <Grid container
      direction='column'
      className={classes.mainContainer}>

      {/* Container Wrapping first section with heading, body and arrows to navigate */}
      <Grid item container
        direction='row'
        // if on the medium breakpoint or below!! center if on larger >> stays where is..
        justify={matchesMD ? 'center' : undefined}>
        {/* Arrow button back */}
        <Hidden smDown>
          <Grid item
            className={classes.arrowContainer}
            style={{ marginRight: '1em', marginLeft: '-3.5em' }}>
            <Link to='/services/'>
              <IconButton style={{ background: 'transparent' }}>
                <img src={backArrow} alt='Back to services page' />
              </IconButton>
            </Link>
          </Grid>
        </Hidden>

        {/* Container holding heading and body items */}
        <Grid item container
          direction='column'
          className={classes.heading}>
          <Grid item>
            <Typography variant='h1'
              align={matchesMD ? 'center' : undefined}
              style={{ marginBottom: '1em' }}>
              Custom Software Development
            </Typography>
          </Grid>

          <Grid item>
            <Typography
              variant='body1'
              paragraph>
              …And the fluffy kitten played with that ball of string all through the night. On a lighter note, a Kwik-E-Mart clerk was brutally murdered last night. Last night's "Itchy and Scratchy Show" was, without a doubt, the worst episode *ever.* Rest assured, I was on the Internet within minutes, registering my disgust throughout the world.
            </Typography>
          </Grid>

          <Grid item>
            <Typography
              variant='body1'
              paragraph>
              He didn't give you gay, did he? Did he?! Kids, we need to talk for a moment about Krusty Brand Chew Goo Gum Like Substance. We all knew it contained spider eggs, but the hantavirus? That came out of left field. So if you're experiencing numbness and/or comas, send five dollars to antidote, PO box…
            </Typography>
          </Grid>

          <Grid item>
            <Typography
              variant='body1'
              paragraph>
              I was saying "Boo-urns. " I'm allergic to bee stings. They cause me to, uh, die. D'oh. Shoplifting is a victimless crime. Like punching someone in the dark. Please do not offer my god a peanut.
            </Typography>
          </Grid>
        </Grid>

        {/* Forward arrow */}
        <Hidden smDown>
          <Link to='/ios_android'>
            <Grid item className={classes.arrowContainer}>
              <IconButton style={{ background: 'transparent' }}>
                <img src={forwardArrow} alt='Forward to iOS Android App Development Page' />
              </IconButton>
            </Grid>
          </Link>
        </Hidden>
      </Grid>

      {/* md means that columns are shared equally till medium breakpoint */}

      {/* Container holding icons with text for saving energy and so on... */}
      <Grid item container
        direction='row'
        style={{ marginTop: '8em', marginBottom: '14em' }}
        justify='center'>
        <Grid item container
          direction='column'
          style={{ maxWidth: '30em' }}
          alignItems='center' md>
          <Grid item>
            <Typography variant='h4'>Save Energy</Typography>
          </Grid>
          <Grid item>
            <img src={lightbulb} alt='lightbulb' />
          </Grid>
        </Grid>

        <Grid item container
          direction='column'
          alignItems='center'
          style={{
            marginTop: matchesSM ? '6em' : 0,
            marginBottom: matchesSM ? '6em' : 0,
            maxWidth: '30em'
          }} md>
          <Grid item>
            <Typography variant='h4'>Save Cash</Typography>
          </Grid>
          <Grid item>
            <img src={cash} alt='cash' />
          </Grid>
        </Grid>

        <Grid item container
          direction='column'
          style={{ maxWidth: '30em' }}
          alignItems='center' md>
          <Grid item>
            <Typography variant='h4'>Save Time</Typography>
          </Grid>
          <Grid item>
            <img src={stopwatch} alt='stopwatch' />
          </Grid>
        </Grid>
      </Grid>


      {/* Container for two animations side by side */}
      <Grid item container
        direction={matchesMD ? 'column' : 'row'}
        alignItems={matchesMD ? 'center' : undefined}
        justify='space-around'>

        {/* First column with animation */}
        <Grid item container
          className={classes.itemContainer}
          direction={matchesSM ? 'column' : 'row'}
          style={{ marginBottom: matchesMD ? '12em' : 0 }} md>
          <Grid item container
            direction='column' md>
            <Grid item>
              <Typography variant='h4'
                align={matchesMD ? 'center' : undefined}>
                Digital Documents and Data.
              </Typography>
            </Grid>
            <Grid item>
              <Typography variant='body1'
                align={matchesMD ? 'center' : undefined}
                paragraph>
                Save me, Jeebus. Well, he's kind of had it in for me ever since I accidentally ran over his dog. Actually, replace "accidentally" with "repeatedly" and replace "dog" with "son." Yes! I am a citizen! Now which way to the welfare office? I'm kidding, I'm kidding. I work, I work.
              </Typography>
            </Grid>
          </Grid>
          <Grid item md>
            <Lottie options={documentsOptions}
              isStopped={true}
              style={{ maxWidth: 275, maxHeight: 275, minHeight: 250 }} />
          </Grid>
        </Grid>

        {/* Second column with animation */}
        <Grid item container
          className={classes.itemContainer}
          direction={matchesSM ? 'column' : 'row'} md>
          <Grid item md>
            <Lottie
              options={scaleOptions}
              style={{ maxHeight: 260, maxWidth: 280 }} />
          </Grid>
          <Grid item container
            direction='column' md>
            <Grid item>
              <Typography variant='h4'
                align={matchesMD ? 'center' : 'right'}>
                Scale
              </Typography>
            </Grid>
            <Grid item>
              <Typography variant='body1'
                align={matchesMD ? 'center' : 'right'}
                paragraph>
                Save me, Jeebus. Well, he's kind of had it in for me ever since I accidentally ran over his dog. Actually, replace "accidentally" with "repeatedly" and replace "dog" with "son." Yes! I am a citizen! Now which way to the welfare office? I'm kidding, I'm kidding. I work, I work.
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>



      {/* Section with tree */}
      <Grid item container
        direction='row'
        style={{ marginTop: '14em', marginBottom: '14em' }}>
        <Grid item container
          alignItems='center'
          direction='column'>
          <Grid item>
            <img src={roots}
              alt="tree with roots extending out..."
              height={matchesMD ? '300' : '450em'}
              width={matchesMD ? '300' : '450em'} />
          </Grid>
          <Grid item className={classes.itemContainer}>
            <Typography variant='h4'
              align='center'
              gutterBottom>
              Root-Cause Analysis
            </Typography>
            <Typography variant='body1'
              align='center'
              paragraph>
              Fire can be our friend; whether it's toasting marshmallows or raining down on Charlie. "Thank the Lord"? That sounded like a prayer. A prayer in a public school.
            </Typography>
            <Typography variant='body1'
              align='center'
              paragraph>
              The Internet King? I wonder if he could provide faster nudity… Son, a woman is like a beer. They smell good, they look good, you'd step over your own mother just to get one! But you can't stop at one. You wanna drink another woman!
            </Typography>
          </Grid>
        </Grid>
      </Grid>

      {/* Container for holding two containers side by side */}
      <Grid item container
        direction={matchesMD ? 'column' : 'row'}
        alignItems={matchesMD ? 'center' : undefined}
        justify='space-around'
        marginBottom='10em'>

        {/* third column with animation */}
        <Grid item container className={classes.itemContainer}
          direction={matchesSM ? 'column' : 'row'}
          style={{ marginBottom: matchesMD ? '12em' : 0 }} md>
          <Grid item container direction='column' md>
            <Grid item>
              <Typography variant='h4'
                align={matchesMD ? 'center' : undefined}>
                Automation
              </Typography>
            </Grid>
            <Grid item>
              <Typography variant='body1'
                align={matchesMD ? 'center' : undefined}
                paragraph>
                Fire can be our friend; whether it's toasting marshmallows or raining down on Charlie. "Thank the Lord"?
              </Typography>
              <Typography variant='body1'
                align={matchesMD ? 'center' : undefined}
                paragraph>
                Yes! I am a citizen! Now which way to the welfare office? I'm kidding, I'm kidding. I work, I work. Marge, just about everything's a sin. Y'ever sat down and read this thing? Technically we're not supposed to go to the bathroom.
              </Typography>
            </Grid>
          </Grid>
          <Grid item md>
            <Lottie options={automationOptions}
              isStopped={true}
              style={{ maxWidth: 280, maxHeight: 290 }} />
          </Grid>
        </Grid>

        {/* fourth column with animation */}
        <Grid item container className={classes.itemContainer}
          direction={matchesSM ? 'column' : 'row'} md>
          <Grid item style={{ marginBottom: '2em' }} md>
            <Lottie options={uxOptions}
              style={{ maxHeight: 310, maxWidth: 155 }} />
          </Grid>
          <Grid item container direction='column' md>
            <Grid item>
              <Typography variant='h4'
                align={matchesMD ? 'center' : 'right'}>
                User Experience Design
              </Typography>
            </Grid>
            <Grid item>
              <Typography variant='body1'
                align={matchesMD ? 'center' : 'right'}
                paragraph>
                Here's to alcohol, the cause of — and solution to — all life's problems. How is education supposed to make me feel smarter? Besides, every time I learn something new, it pushes some old stuff out of my brain. Remember when I took that home winemaking course, and I forgot how to drive?
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Grid item style={{ marginBottom: '3em', marginTop: '3em' }}>
        <CTA setValue={props.setValue} />
      </Grid>
    </Grid>

  )
}

export default CustomSoftware
