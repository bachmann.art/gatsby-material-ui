import React from 'react'
import Lottie from 'react-lottie'
import { Link } from 'gatsby'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import IconButton from '@material-ui/core/IconButton'
import Hidden from '@material-ui/core/Hidden'

import backArrow from '../../../assets/backArrow.svg'
import forwardArrow from '../../../assets/forwardArrow.svg'
import swiss from '../../../assets/swissKnife.svg'
import access from '../../../assets/extendAccess.svg'
import engagement from '../../../assets/increaseEngagement.svg'

import integrationAnimation from '../../../animations/integrationAnimation/data.json'

import CTA from '../CTA'


const useStyles = makeStyles(theme => ({
  rowContainer: {
    paddingLeft: '4em',
    paddingRight: '4em',
    paddingTop: '2em',
    paddingBottom: '7em',
    [theme.breakpoints.down('sm')]: {
      paddingLeft: '1.2em',
      paddingRight: '1.2em',
      paddingBottom: '1em',
      paddingTop: '1em'
    }
  },
  heading: {
    maxWidth: '40em',
  },
  arrowContainer: {
    marginTop: '0.5em'
  },
  itemContainer: {
    maxWidth: '40em'
  }
}))

const IOSAndroidPage = (props) => {

  const classes = useStyles()
  const theme = useTheme()
  const matchesMD = useMediaQuery(theme.breakpoints.down('md'))
  const matchesSM = useMediaQuery(theme.breakpoints.down('sm'))

  const defaultOptions = {
    loop: true,
    autoplay: false,
    animationData: integrationAnimation,
    renderSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  }

  return (
    <Grid container direction='column'>
      <Grid item container
        direction='row'
        // if on the medium breakpoint or below!! center if on larger >> stays where is..
        justify={matchesMD ? 'center' : undefined}>
        {/* Arrow button back */}
        <Hidden smDown>
          <Grid item
            className={classes.arrowContainer}
            style={{ marginRight: '1em', marginLeft: '-3.5em' }}>
            <Link to='/customsoftware/'>
              <IconButton style={{ background: 'transparent' }}>
                <img src={backArrow} alt='Back to Custom Software Page' />
              </IconButton>
            </Link>
          </Grid>
        </Hidden>

        {/* Container holding heading and body items */}
        <Grid item container
          direction='column'
          className={classes.heading}>
          <Grid item>
            <Typography
              variant='h1'
              align={matchesMD ? 'center' : undefined}
              style={{ marginBottom: '1em' }}>
              iOS/Android App Development
            </Typography>
          </Grid>

          <Grid item>
            <Typography
              variant='body1'
              paragraph>
              …And the fluffy kitten played with that ball of string all through the night. On a lighter note, a Kwik-E-Mart clerk was brutally murdered last night. Last night's "Itchy and Scratchy Show" was, without a doubt, the worst episode *ever.*
            </Typography>
          </Grid>

          <Grid item>
            <Typography
              variant='body1'
              paragraph>
              He didn't give you gay, did he? Did he?! Kids, we need to talk for a moment about Krusty Brand Chew Goo Gum Like Substance. We all knew it contained spider eggs, but the hantavirus?
            </Typography>
          </Grid>

          <Grid item>
            <Typography
              variant='body1'
              paragraph>
              I was saying "Boo-urns. " I'm allergic to bee stings. They cause me to, uh, die.
            </Typography>
          </Grid>
        </Grid>

        {/* Forward arrow */}
        <Hidden smDown>
          <Link to='/web_development/'>
            <Grid item className={classes.arrowContainer}>
              <IconButton style={{ background: 'transparent' }}>
                <img src={forwardArrow} alt='Forward to Web Development Page' />
              </IconButton>
            </Grid>
          </Link>
        </Hidden>
      </Grid>

      {/* Container of three columns picture in the middle */}
      <Grid item container
        direction={matchesSM ? 'column' : 'row'}
        style={{ marginTop: '12em', marginBottom: '12em' }}
        className={classes.rowContainer}>

        {/* First text column */}
        <Grid item container
          direction="column" md>
          <Grid item>
            <Typography variant='h4'
              align={matchesSM ? 'center' : undefined}
              gutterBottom>
              Integration
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant='body1'
              align={matchesSM ? 'center' : undefined}
              paragraph
              gutterBottom>
              'm going to the back seat of my car, with the woman I love, and I won't be back for ten minutes! Me fail English? That's unpossible. Beer. Now there's a temporary solution. I don't like being outdoors, Smithers. For one thing, there's too many fat children.
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant='body1'
              align={matchesSM ? 'center' : undefined}
              paragraph
              gutterBottom>
              Oh, so they have Internet on computers now! This is the greatest case of false advertising I've seen since I sued the movie "The Never Ending Story." Marge, it takes two to lie. One to lie and one to listen.
            </Typography>
          </Grid>
        </Grid>

        {/* Animation column in the middle */}
        <Grid item md>
          <Lottie options={defaultOptions}
            style={{ maxWidth: '20em' }}
            isStopped={true} />
        </Grid>

        {/* Second text column  */}
        <Grid item container
          direction="column" md>
          <Grid item>
            <Typography variant='h4'
              align={matchesSM ? 'center' : "right"}
              gutterBottom>
              Simultaneous Platform Support.
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant='body1'
              align={matchesSM ? 'center' : "right"}
              paragraph
              gutterBottom>
              I'm going to the back seat of my car, with the woman I love, and I won't be back for ten minutes! Me fail English? That's unpossible. Beer. Now there's a temporary solution. I don't like being outdoors, Smithers. For one thing, there's too many fat children.
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant='body1'
              align={matchesSM ? 'center' : "right"}
              paragraph
              gutterBottom>
              Oh, so they have Internet on computers now! This is the greatest case of false advertising I've seen since I sued the movie "The Never Ending Story." Marge, it takes two to lie. One to lie and one to listen.
            </Typography>
          </Grid>
        </Grid>
      </Grid>

      {/* Container with three columns of Header + picture */}
      <Grid item container
        direction={matchesMD ? 'column' : 'row'}
        className={classes.rowContainer}
        style={{ marginBottom: '8em' }}>
        <Grid item container
          alignItems='center'
          direction='column' md>
          <Grid item>
            <Typography variant='h4'
              gutterBottom>
              Extended Functionality.
            </Typography>
          </Grid>
          <img src={swiss} alt='Swiss army knife'
            style={{ maxWidth: '28em' }} />
        </Grid>
        <Grid item container
          alignItems='center'
          direction='column' md
          style={{ marginTop: matchesMD ? '8em' : 0, marginBottom: matchesMD ? '8em' : 0 }}>
          <Grid item>
            <Typography variant='h4'
              gutterBottom>
              Extended Access.
          </Typography>
          </Grid>
          <img src={access} alt='Extended Access'
            style={{ maxWidth: matchesSM ? '20em' : '28em' }} />
        </Grid>
        <Grid item container
          alignItems='center'
          direction='column' md>
          <Grid item>
            <Typography variant='h4'
              gutterBottom>
              Increase Engagement.
            </Typography>
          </Grid>
          <img src={engagement} alt='Increase Engagement'
            style={{ maxWidth: '28em' }} />
        </Grid>
      </Grid>
      <Grid item>
        <CTA setValue={props.setValue} />
      </Grid>
    </Grid >
  )
}

export default IOSAndroidPage
