import React from 'react'
import Hero from './Hero'
import Services from './Services'
import Mobileapps from './Mobileapps'
import Website from './Website'
import Revolution from './Revolution'
import InfoBlock from './InfoBlock'
import CTA from '../CTA'


const LandingPage = () => {
  return (
    <>
      <Hero />
      <Services />
      <Mobileapps />
      <Website />
      <Revolution />
      <InfoBlock />
      <CTA />
    </>
  )
}

export default LandingPage
