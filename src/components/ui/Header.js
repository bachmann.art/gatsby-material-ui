
import React, { useState } from 'react'
import { Link } from 'gatsby'
import links from '../../constants/links'
import styles from "../../css/navbar.module.css"
import { FaAlignRight } from "react-icons/fa"


const Header = () => {
  const [isOpen, setNav] = useState(false)
  const toggleNav = () => {
    setNav(isOpen => !isOpen)
  }
  return (
    <nav className={styles.navbar}>
      <div className={styles.navCenter}>
        <div className={styles.navHeader}>
          <button type="button" className={styles.logoBtn} onClick={toggleNav}>
            <FaAlignRight className={styles.logoIcon} />
          </button>
        </div>
        <ul
          className={
            isOpen
              ? `${styles.navLinks} ${styles.showNav}`
              : `${styles.navLinks}`
          }
        >
          {links.map((item, index) => {
            return (
              <li key={index}>
                <Link fade to={item.path}>
                  {item.text}
                </Link>
              </li>
            )
          })}
        </ul>
      </div>
    </nav>
  )
}

export default Header


























// import Typography from '@material-ui/core/Typography'
// import AppBar from '@material-ui/core/AppBar'
// import Toolbar from '@material-ui/core/Toolbar'
// import useScrollTrigger from '@material-ui/core/useScrollTrigger'
// import { makeStyles } from '@material-ui/styles'
// import Tabs from '@material-ui/core/Tabs';
// import Tab from '@material-ui/core/Tab';
// import Button from '@material-ui/core/Button'

// //import logo from '../../assets/myLogos/main_logo.png'

// function ElevationScroll(props) {
//   const { children } = props

//   const trigger = useScrollTrigger({
//     disableHysteresis: true,
//     threshold: 0,
//   });

//   return React.cloneElement(children, {
//     elevation: trigger ? 4 : 0,
//   });
// }

// const useStyles = makeStyles(theme => ({
//   toolbarMargin: {
//     ...theme.mixins.toolbar,
//     marginBottom: '2em'
//   },
//   logo: {
//     height: '6em'
//   },
//   tabContainer: {
//     marginLeft: 'auto',
//     indicatorColor: 'common'
//   },
//   tab: {
//     ...theme.typography.tab,
//     minWidth: 10,
//     marginLeft: '25px'
//   },
//   button: {
//     ...theme.typography.estimate,
//     borderRadius: '50px',
//     marginLeft: '40px',
//     marginRight: '30px',
//     letterSpacing: '3px',
//     fontWeight: 600
//   }
// }))

// const Header = (props) => {
//   const classes = useStyles()
//   const [value, setValue] = useState(0)

//   const handleChange = (e, value) => {
//     setValue(value)
//   }

//   return (
//     <>
//       <ElevationScroll>
//         <AppBar position='fixed'
//           color='primary'>
//           <Toolbar disableGutters>
//             <Tabs
//               value={value}
//               onChange={handleChange}
//               className={classes.tabContainer}
//             >

//               <Tab className={classes.tab} label='Home' />
//               <Tab className={classes.tab} label='Revolution' />
//               <Tab className={classes.tab} label='About Us' />
//               <Tab className={classes.tab} label='Contact Us' />
//             </Tabs>
//             <Button variant='contained'
//               color='secondary'
//               className={classes.button}
//             >
//               Free Estimate
//             </Button>
//           </Toolbar>
//         </AppBar>
//       </ElevationScroll>
//       <div className={classes.toolbarMargin} />
//     </>
//   )
// }

// export default Header








































// import PropTypes from 'prop-types'
// import React from 'react'

// import { Link } from 'gatsby'

// import clsx from 'clsx'
// import { makeStyles, useTheme } from '@material-ui/core/styles'
// import Drawer from '@material-ui/core/Drawer'
// import CssBaseline from '@material-ui/core/CssBaseline'
// import AppBar from '@material-ui/core/AppBar'
// import Toolbar from '@material-ui/core/Toolbar'
// import List from '@material-ui/core/List'
// import Typography from '@material-ui/core/Typography'
// import Divider from '@material-ui/core/Divider'
// import IconButton from '@material-ui/core/IconButton'
// import MenuIcon from '@material-ui/icons/Menu'
// import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
// import ChevronRightIcon from '@material-ui/icons/ChevronRight'
// import ListItem from '@material-ui/core/ListItem'
// import ListItemIcon from '@material-ui/core/ListItemIcon'
// import ListItemText from '@material-ui/core/ListItemText'
// import HomeIcon from '@material-ui/icons/Home'
// import ListIcon from '@material-ui/icons/ViewList'

// const drawerWidth = 240

// const useStyles = makeStyles(theme => ({
//   root: {
//     display: 'flex',
//   },
//   appBar: {
//     transition: theme.transitions.create(['margin', 'width'], {
//       easing: theme.transitions.easing.sharp,
//       duration: theme.transitions.duration.leavingScreen,
//     }),
//     background: 'linear-gradient(to right,  #663399, #5B72FF)',
//   },
//   appBarShift: {
//     width: `calc(100% - ${drawerWidth}px)`,
//     marginLeft: drawerWidth,
//     transition: theme.transitions.create(['margin', 'width'], {
//       easing: theme.transitions.easing.easeOut,
//       duration: theme.transitions.duration.enteringScreen,
//     }),
//   },
//   menuButton: {
//     marginRight: theme.spacing(2),
//   },
//   hide: {
//     display: 'none',
//   },
//   drawer: {
//     width: drawerWidth,
//     flexShrink: 0,
//   },
//   drawerPaper: {
//     width: drawerWidth,
//   },
//   drawerHeader: {
//     display: 'flex',
//     alignItems: 'center',
//     padding: '0 8px',
//     ...theme.mixins.toolbar,
//     justifyContent: 'flex-end',
//   },
//   content: {
//     flexGrow: 1,
//     padding: theme.spacing(3),
//     transition: theme.transitions.create('margin', {
//       easing: theme.transitions.easing.sharp,
//       duration: theme.transitions.duration.leavingScreen,
//     }),
//     marginLeft: -drawerWidth,
//   },
//   contentShift: {
//     transition: theme.transitions.create('margin', {
//       easing: theme.transitions.easing.easeOut,
//       duration: theme.transitions.duration.enteringScreen,
//     }),
//     marginLeft: 0,
//   },
// }))

// const Header = ({ siteTitle }) => {
//   const classes = useStyles()

//   const theme = useTheme()
//   const [open, setOpen] = React.useState(false)

//   function handleDrawerOpen() {
//     setOpen(true)
//   }

//   function handleDrawerClose() {
//     setOpen(false)
//   }

//   return (
//     <div className={classes.root}>
//       <CssBaseline />
//       <AppBar
//         position="fixed"
//         elevation={0}
//         className={clsx(classes.appBar, {
//           [classes.appBarShift]: open,
//         })}
//       >
//         <Toolbar>
//           <IconButton
//             color="inherit"
//             aria-label="Open drawer"
//             onClick={handleDrawerOpen}
//             edge="start"
//             className={clsx(classes.menuButton, open && classes.hide)}
//           >
//             <MenuIcon />
//           </IconButton>
//           <Typography variant="h6" color="inherit">
//             {siteTitle}
//           </Typography>
//         </Toolbar>
//       </AppBar>
//       <Drawer
//         className={classes.drawer}
//         variant="persistent"
//         anchor="left"
//         open={open}
//         classes={{
//           paper: classes.drawerPaper,
//         }}
//       >
//         <div className={classes.drawerHeader}>
//           <IconButton onClick={handleDrawerClose}>
//             {theme.direction === 'ltr' ? (
//               <ChevronLeftIcon />
//             ) : (
//               <ChevronRightIcon />
//             )}
//           </IconButton>
//         </div>
//         <Divider />
//         <List>
//           <Link to="/">
//             <ListItem button>
//               <ListItemIcon>
//                 <HomeIcon />
//               </ListItemIcon>
//               <ListItemText>Home</ListItemText>
//             </ListItem>
//           </Link>
//           <Link to="/components">
//             <ListItem button>
//               <ListItemIcon>
//                 <ListIcon />
//               </ListItemIcon>
//               <ListItemText>Components</ListItemText>
//             </ListItem>
//           </Link>
//         </List>
//       </Drawer>
//     </div>
//   )
// }

// Header.propTypes = {
//   siteTitle: PropTypes.string,
// }

// Header.defaultProps = {
//   siteTitle: ``,
// }

// export default Header
