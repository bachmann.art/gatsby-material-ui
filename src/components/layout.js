import React from 'react'
import { ThemeProvider } from '@material-ui/styles'

import PortfolioHeaderFull from './ui/PortfolioHeaderFull'
//import PortfolioHeaderMobile from './ui/PortfolioHeaderMobile'
import theme from './ui/Theme'
import Header from './ui/Header'
import Footer from './ui/Footer'

const Layout = ({ children }) => {
  return (
    <ThemeProvider theme={theme}>
      {/* <PortfolioHeaderMobile /> */}
      <PortfolioHeaderFull />
      <Header />
      {children}
      <Footer />
    </ThemeProvider>
  )
}

export default Layout
